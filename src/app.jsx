import ReactDOM from "react-dom";
import React from "react";

import * as bitOperations from "./bitOperations";

const SUBNET_MAX = 32
const MAX_IPV4 = 2 ** 32;

function validateIP(address) {
    return /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(address);
}

function validateSubnet(subnet) {
    return subnet > 0 && subnet <= SUBNET_MAX;
}

const subnetToMask = (function(subnet) {
    const memo = {};
    function f(s) {
        if (!(s in memo)) {
            memo[s] = MAX_IPV4 - ((MAX_IPV4) / (2 ** s));
        }
        return memo[s];
    }
    return f;
})();

function ipv4ToNumber(address) {
    const parts = address.split(".").map(Number);
    return parts[3] + 256 * (parts[2] + 256 * (parts[1] + 256 * parts[0]));
}

function numberToIpv4(number) {
    const fourth = number % 256;
    const third = Math.max((number % (256 ** 2) - fourth), 0);
    const second = Math.max((number % (256 ** 3) - third - fourth), 0);
    const first = Math.max((number % (256 ** 4) - second - third - fourth), 0);
    return `${first / (256 ** 3)}.${second / (256 ** 2)}.${third / 256}.${fourth}`;
}

function rangeEnds(addressNumber, maskNumber) {
    let network = bitOperations.and(addressNumber, maskNumber);
    let broadcast = bitOperations.or(network, ~maskNumber);
    return {network, broadcast};
}

function combineColour(r, g, b) {
    return `rgba(${r},${g},${b},0.5`;
}

class Page extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ranges: [null, null]
        }
        this.rangeChanged = this.rangeChanged.bind(this);
    }

    rangeChanged(key, value) {
        this.setState(state => {
            state.ranges[key] = value;
            return state
        })
    }

    render() {
        return (
            <div className="row">
                <div className="col-sm-12">Subnet Visualiser</div>
                <div className="col-sm-6">
                    {this.state.ranges.map((val, key) => <Subnet key={key} componentKey={key} rangeChanged={this.rangeChanged} />)}
                </div>
                <div className="col-sm-6">
                    <RangeVisualiser ranges={this.state.ranges} />
                </div>
            </div>
        );
    }
}

class Subnet extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            key: this.props.componentKey,
            address: "192.168.1.1",
            subnet: 24,
            red: 0,
            green: 255,
            blue: 0,
            testAddress: ""
        }
        this.props.rangeChanged(this.props.componentKey, this.state);
    }

    inputChange(name, value) {
        this.setState(state => {
            Object.assign(state, {[name]: value})
            if (name !== "testAddress") this.props.rangeChanged(this.props.componentKey, state);
            return state;
        });
    }

    render() {
        const ipValid = validateIP(this.state.address);
        const subnetValid = validateSubnet(this.state.subnet);
        let broadcastAddress = "";
        let networkAddress = "";
        let testClass = "form-control col-sm-6 ";
        let mask = -2;
        if (validateSubnet(this.state.subnet) && validateIP(this.state.address)) {
            mask = subnetToMask(this.state.subnet);
            const addressNumber = ipv4ToNumber(this.state.address);
            const endPoints = rangeEnds(addressNumber, mask);
            networkAddress = numberToIpv4(endPoints.network);
            broadcastAddress = numberToIpv4(endPoints.broadcast);
            if (validateIP(this.state.testAddress)) {
                const testAddressNumber = ipv4ToNumber(this.state.testAddress);
                if (testAddressNumber < endPoints.broadcast && testAddressNumber > endPoints.network) {
                    testClass += "is-valid";
                }
                else {
                    testClass += "is-invalid";
                }
            }
        }
        return (
            <div className="row" style={{margin: "15px"}}>
                <label className="col-sm-3 col-form-label">Interface</label>
                <input className={`form-control col-sm-4 ${ipValid ? "is-valid" : "is-invalid"}`} type="text" value={this.state.address} onChange={evt => this.inputChange("address", evt.target.value.trim())} />
                <input type="text" readOnly className=" col-sm-1 form-control-plaintext" value={"/"} />
                <input className={`form-control col-sm-2  ${subnetValid ? "is-valid" : "is-invalid"}`} type="text" value={this.state.subnet} onChange={evt => this.inputChange("subnet", parseInt(evt.target.value, 10))} />
                {!ipValid ? <div className="invalid-feedback col-sm-9 offset-sm-3">Interface must be a valid IP address</div> : ""}
                {!subnetValid ? <div className="invalid-feedback col-sm-9 offset-sm-3">Subnet must be between 1 and {SUBNET_MAX}</div> : ""}

                <label className="col-sm-6 col-form-label">Network Address</label>
                <div className="col-sm-6">
                    <input type="text" readOnly className="form-control-plaintext" value={networkAddress} />
                </div>
                <label className="col-sm-6 col-form-label">Broadcast Address</label>
                <div className="col-sm-6">
                    <input type="text" readOnly className="form-control-plaintext" value={broadcastAddress} />
                </div>
                <label className="col-sm-6 col-form-label">Addresses in subnet</label>
                <div className="col-sm-6">
                    <input type="text" readOnly className="form-control-plaintext" value={~mask - 1} />
                </div>

                <label className="col-sm-3 col-form-label">Test</label>
                <input type="text" className={testClass} value={this.state.testAddress} onChange={evt => this.inputChange("testAddress", evt.target.value.trim())} />
                <div className="w-100 mb-3"></div>

                <label className="col-sm-3 col-form-label">Colour</label>
                <div className="input-group col-sm-3">
                    <div className="input-group-prepend"><div className="input-group-text">R</div></div>
                    <input type="text" className="form-control form-control-sm" value={this.state.red} onChange={evt => this.inputChange("red", evt.target.value.trim())}/>
                </div>
                <div className="input-group col-sm-3">
                    <div className="input-group-prepend"><div className="input-group-text">G</div></div>
                    <input type="text" className="form-control form-control-sm" value={this.state.green} onChange={evt => this.inputChange("green", evt.target.value.trim())}/>
                </div>
                <div className="input-group col-sm-3">
                    <div className="input-group-prepend"><div className="input-group-text">B</div></div>
                    <input type="text" className="form-control form-control-sm" value={this.state.blue} onChange={evt => this.inputChange("blue", evt.target.value.trim())}/>
                </div>
                <RangeVisualiser ranges={[this.state]} />
            </div>
        );
    }
}

class RangeVisualiser extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        const ranges = [];
        const scaledRanges = [];
        const scaledRangesDivs = [];
        const indicatorWidth = 100;
        const height = 25;
        let rangeStart = 2 ** 32;
        let rangeEnd = 0;
        for (const range of this.props.ranges) {
            if (range === null) continue;
            if (validateSubnet(range.subnet) && validateIP(range.address)) {
                const mask = subnetToMask(range.subnet);
                const addressNumber = ipv4ToNumber(range.address);
                const {network: networkAddress, broadcast: broadcastAddress} = rangeEnds(addressNumber, mask);
                let testAddressNumber = 0;
                let testAddressDiv = null;
                if (validateIP(range.testAddress)) {
                    testAddressNumber = ipv4ToNumber(range.testAddress);
                    const testLeft = indicatorWidth * testAddressNumber / MAX_IPV4 + "%";
                    testAddressDiv = <div style={{backgroundColor: "#777777", position: "absolute", left: testLeft, height, width: 1}}></div>
                }
                // console.log(addressNumber, mask, networkAddress, broadcastAddress);
                // console.log(addressNumber.toString(2))
                // console.log(mask.toString(2))
                // console.log(networkAddress.toString(2))
                // console.log(broadcastAddress.toString(2))
                const left = indicatorWidth * networkAddress / MAX_IPV4 + "%";
                const width = indicatorWidth * (MAX_IPV4 - mask) / MAX_IPV4 + "%";
                const addressLocation = indicatorWidth * addressNumber / MAX_IPV4 + "%";

                if (networkAddress < rangeStart) rangeStart = bitOperations.and(networkAddress, 2 ** 32 - 2 ** 8);
                if (broadcastAddress > rangeEnd) rangeEnd = bitOperations.or(broadcastAddress, 255);
                ranges.push((
                    <div key={range.key + "-" + range.address + "/" + range.subnet}>
                        <div style={{backgroundColor: combineColour(range.red, range.green, range.blue), height, position: "absolute", left, width }}></div>
                        <div style={{backgroundColor: "#000000", position: "absolute", left: addressLocation, height, width: 1}}></div>
                        {testAddressDiv}
                    </div>
                ));
                scaledRanges.push({
                    key: range.key + "-" + range.address + "/" + range.subnet,
                    networkAddress,
                    broadcastAddress,
                    addressNumber,
                    red: range.red,
                    green: range.green,
                    blue: range.blue,
                    testAddressNumber
                });
            }
            else {
                console.log("something was invalid")
            }
        }
        if (rangeStart < rangeEnd) {
            const diff = rangeEnd - rangeStart;
            for (const range of scaledRanges) {
                const left = (range.networkAddress - rangeStart) / diff;
                const width = (range.broadcastAddress - rangeStart) / diff - left;
                const address = 100 * (range.addressNumber - rangeStart) / diff + "%";
                let testAddressDiv = null;
                // console.log(rangeStart, rangeEnd, diff, range.networkAddress, range.broadcastAddress, left, width, address);
                if (range.testAddressNumber > range.networkAddress && range.testAddressNumber < range.broadcastAddress) {
                    const testLeft = 100 * (range.testAddressNumber - rangeStart) / diff + "%";
                    testAddressDiv = <div style={{backgroundColor: "#777777", position: "absolute", left: testLeft, height, width: 1}}></div>
                }
                scaledRangesDivs.push(
                    <div key={range.key}>
                        <div style={{backgroundColor: combineColour(range.red, range.green, range.blue), height, position: "absolute", left: 100 * left + "%", width: 100 * width + "%" }}></div>
                        <div style={{backgroundColor: "#000000", position: "absolute", left: address, height, width: 1}}></div>
                        {testAddressDiv}
                    </div>
                )
            }
        }
        return (
            <div className="col-sm-12">
                <div style={{width: indicatorWidth + "%", height, position: "relative", backgroundColor: "#DDDDDD", margin: "15px 0px"}}>
                    {ranges}
                </div>
                <div style={{width: indicatorWidth + "%", height, position: "relative", backgroundColor: "#DDDDDD"}}>
                    {scaledRangesDivs}
                </div>
                <div>
                    <span style={{float: "left"}}>{numberToIpv4(rangeStart)}</span>
                    <span style={{float: "right"}}>{numberToIpv4(rangeEnd)}</span>
                </div>
            </div>
        )
    }
}

ReactDOM.render(
    <Page />,
    document.getElementById("root")
);