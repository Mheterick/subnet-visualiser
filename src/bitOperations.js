// For bit operations javascript converts operands to 32 bit signed integers, this causes issues
// when the operands are intended to be 32 bits or bigger

const wordSize = 2 ** 31;

function splitOut(a, b) {
    const aHI = a / wordSize;
    const aLO = a % wordSize;
    const bHI = b / wordSize;
    const bLO = b % wordSize;
    return {
        aHI: a / wordSize,
        aLO: a % wordSize,
        bHI: b / wordSize,
        bLO: b % wordSize
    }
}

function and(a, b) {
    const s = splitOut(a, b);
    return ((s.aHI & s.bHI) * wordSize + (s.aLO & s.bLO))
}

function or(a, b) {
    const s = splitOut(a, b);
    return ((s.aHI | s.bHI) * wordSize + (s.aLO | s.bLO))
}

export {and, or};