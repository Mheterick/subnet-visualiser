const webpack = require("webpack");
const path = require("path");

const BUILD_DIR = path.resolve(__dirname, "");
const APP_DIR = path.resolve(__dirname, "src");

module.exports = {
    entry: APP_DIR + "/app.jsx",
    output: {
        path: BUILD_DIR,
        filename: "index.js"
    },
    module: {
        loaders: [
            {
                test: /\.jsx?/,
                include: APP_DIR,
                loader: "babel-loader"
            }
        ]
    }
};